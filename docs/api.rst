Synchronous API
===

.. automodule:: skalman.PATH
.. automodule:: skalman.bin
.. automodule:: skalman.command
.. automodule:: skalman.file
.. automodule:: skalman.flag
.. automodule:: skalman.flags
.. automodule:: skalman.process
.. automodule:: skalman.process_io
.. automodule:: skalman.redirect
.. automodule:: skalman.temp

Async API
===
.. automodule:: skalman.pipeline
