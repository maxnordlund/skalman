"""
This module provides access to all executables found in PATH.

This allows for some pretty neat shell scripting.

>>> from skalman.PATH import ls
>>> ls.file
File("/bin/ls")
"""

from __future__ import annotations

import sys
from types import ModuleType
from typing import TYPE_CHECKING

if TYPE_CHECKING:
    from typing import Type, Sequence
    from .pipeline import Executable


# Must be a list of strings
__all__: list[str] = []


def __getattr__(bin: str) -> Executable:  # type: ignore [empty-body]
    # Teach mypy about the dynamic nature of this module.
    ...


def __dir__() -> Sequence[str]:  # type: ignore [empty-body]
    # Teach mypy about the dynamic nature of this module.
    ...


class PathLookupModule(ModuleType):  # noqa: D101
    # __doc__ taken from the module doc above

    def __dir__(self):
        """Return all executables found in PATH."""
        import os
        from .pipeline import File

        return {
            file.name
            for directory in map(File, os.get_exec_path())
            if directory
            for file in directory * ""
            if file.is_executable()
        }

    def __getattr__(self, bin) -> Executable:
        """Lookup the given `bin` in PATH, and if found, return it as a :class:`executable <Executable>`."""
        return self.__lookup(bin, AttributeError)

    def __getitem__(self, bin) -> Executable:
        """
        Lookup the given `bin` in PATH, and if found, return it as a :class:`executable <Executable>`.

        This is the same as :meth:`__getattr__` but allows any name, not just
        Python properties.
        """
        return self.__lookup(bin, KeyError)

    def __lookup(self, bin, Error: Type[Exception]) -> Executable:
        from .pipeline import Executable

        executable = Executable.which(bin)
        if executable is None:
            raise Error(f"{bin!r} not in PATH")
        else:
            return executable


PathLookupModule.__doc__ = sys.modules[__name__].__doc__
sys.modules[__name__].__class__ = PathLookupModule

# Leave nothing in module scope to ensure we don't accidentally
# shadow any binaries with the same name
del sys, ModuleType, TYPE_CHECKING, __getattr__, __dir__
