"""A set of helpers for manipulating files and running commands."""

__version__ = "0.1.0"

import asyncio
from types import CoroutineType
from typing import Callable


def async_main(main: Callable[[], CoroutineType]):
    """
    Run the given function using :func:`asyncio.run` if it's the main module.

    This does a check similar to the classic `__name__ == "__main__"`.
    """
    if main.__module__ == "__main__":
        return asyncio.run(main())
    else:
        return main
