"""Example of how to use :mod:`skalman`."""

from skalman.file import File

from .flags import d, f, s, v
from .PATH import cut, dig, grep, ls, tr


def main():  # noqa: D103
    print("Get size and name of files")
    for line in ls - "la" | grep(-v, "xr") | tr - s(" ") | cut - d(" ") - f("5,9"):
        print(line)

    print("\n\nLookup wikipedia using dig")
    dig_short = dig() + "short"
    print(*dig_short("en.wikipedia.org"))

    print("\n\nGrep for tool sections in pyproject.toml")
    print(*grep("tool.*", File(".") * ".toml"), sep="\n")


if __name__ == "__main__":
    main()
