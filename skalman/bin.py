"""
Pure python implementation of classic Unix tools.

This makes it a bit easier to write portable scripts.
"""

import os
import shutil
from typing import Optional, Union

from skalman.command import Command

from .temp import TemporaryFile, TemporaryDirectory
from .file import File
from .flags import into_dict


class cd:
    """
    Context manager for temporarily changing the current working directory.

    This can be nested safely, however since changing the working directory
    is done for the currently running process, aka the Python interpreter, it
    is not thread safe.::

        with cd("folder"):
            # Now Python's current working directory is "folder"
            with cd("-"):
                # Now we're back to the directory before
        # Same here, back to the starting directory
    """

    current: File
    previous: Optional[File]

    __previous_directory: str = os.getcwd()

    def __init__(self, directory: Union[os.PathLike, str]):
        """
        Prepare to change the current working directory to the one given.

        Just like `cd`, if you specify `"-"` as the directory it will switch
        back to the previous one.
        """
        if directory == "-":
            directory = self.__previous_directory

        self.current = File(directory)
        self.__previous_directory = os.getcwd()
        self.previous = File(self.__previous_directory)

    def __enter__(self):  # noqa D105
        self.previous = Command.working_directory
        Command.working_directory = self.current
        os.chdir(str(self.current))

    def __exit__(self, exc_type, exc_value, traceback):  # noqa D105
        Command.working_directory = self.previous
        os.chdir(self.previous)


class env:
    """
    Context manager for temporarily changing :data:`os.environ`.

    It works a bit like the unix `env` command, and accepts both `-i` and
    `-u` flags.
    """

    def __init__(self, *flags, **environ):
        """
        Create a new environment ready to be used.

        The `-i` flag clears the environment, effectively starting from a
        clean slate, while any `-u` flags remove that specific variable from
        the environment. Useful for targeted cleaning, like clearing
        `PYTHONHOME`.

        All keyword arguments are added to the new environment, overriding
        any previous definition.
        """
        self.environ = os.environ.copy()

        flags_ = into_dict(flags)
        if "-i" in flags_:
            self.environ.clear()
        elif "-u" in flags_:
            to_be_removed = set(flags_["-u"])

            self.environ = {
                key: value
                for key, value in self.environ.items()
                if key not in to_be_removed
            }

        self.environ.update(environ)

    def __call__(self, bin, *arguments, **flags) -> Command:
        """Run the given command with the given arguments and flags under this environment."""
        command = Command(bin, *arguments, **flags)
        command.environ = self.environ
        return command

    def __enter__(self):  # noqa D105
        self.previous = Command.environ
        Command.environ = self.environ

    def __exit__(self, exc_type, exc_value, traceback):  # noqa D105
        Command.environ = self.previous


def chmod(mode: int, *files: File, h=False):
    """Python version of `chmod`."""
    file: File

    for file in files:
        if h:
            file.path.lchmod(mode)
        else:
            file.path.chmod(mode)


def chown(user, group, target: File, *targets: File):
    """Python version of `chown`."""
    file: File

    for file in (target,) + targets:
        shutil.chown(str(file), user=user, group=group)


def cp(
    source: File,
    *sources: File,
    target: Union[File, str],
    L=None,
    P=None,
    p=True,
    R=False,
):
    """
    Python version of `cp`.

    :param source: file or directory to copy
    :param sources: additional files/directories to copy
    :param target: where to copy to
    :param L: follow symlinks
    :param P: don't follow symlinks, default
    :param p: preserve attributes
    :param R: recursive copy
    """
    if L is None:
        follow_symlinks = P if P is not None else True
    elif P is None:
        follow_symlinks = L
    else:
        raise ValueError("Can't specify both L and P at the same time.")

    target = str(target)
    copy_function = shutil.copy2 if p else shutil.copy

    file: File
    for file in (source,) + sources:
        if R:
            shutil.copytree(
                str(file), target, symlinks=follow_symlinks, copy_function=copy_function
            )
        else:
            copy_function(str(file), target, follow_symlinks=follow_symlinks)


def mkdir(*directories: File, m=None, p=False):
    """Python version of `mkdir`."""
    directory: File

    for directory in directories:
        if m is not None:
            directory.path.mkdir(mode=m, parents=p, exist_ok=True)
        else:
            directory.path.mkdir(parents=p, exist_ok=True)


def mktemp(
    d=False, t: Optional[str] = None
) -> Union[TemporaryDirectory, TemporaryFile]:
    """Python version of `mktemp`."""
    if d:
        return TemporaryDirectory(prefix=t)
    else:
        return TemporaryFile(prefix=t)


def mv(source: File, *sources: File, target: Union[File, str]):
    """Python version of `mv`."""
    for source in (source,) + sources:
        shutil.move(str(source), str(target))


def rm(*files: File, d=False, r=False):
    """Python version of `rm`."""
    file: File

    for file in files:
        if r:
            shutil.rmtree(str(file))
        elif file.path.is_dir():
            if d:
                file.path.rmdir()
            else:
                raise ValueError(f"rm: {file}: is a directory")
        else:
            file.path.unlink()


def rmdir(*directories: File):
    """Python version of `rmdir`."""
    for directory in directories:
        directory.path.rmdir()


def unlink(file: File):
    """Python version of `unlink`."""
    file.path.unlink()


def which(bin: str) -> Command:
    """Python version of `which`."""
    return Command(File(bin))
