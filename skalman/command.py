from __future__ import annotations

import os
import re
import shlex
from contextlib import ExitStack, contextmanager, nullcontext
from subprocess import PIPE
from typing import Any, ContextManager, Dict, List, Optional, Union

from skalman.process import Process

from . import flag
from .file import File, Glob
from .flag import Flag, to_flag
from .helpers import ContextManagerMixin, WaitableThread, delegate
from .process_io import ProcessIO
from .temp import TemporaryDirectory

KEYWORD_ARGUMENT_TO_FLAG_REGEXP = re.compile(r"(?<!\A)_")


class Command(ProcessIO, ContextManagerMixin[Process]):
    """
    Class that represents an executable program.

    This also acts as a builder for such programs to facilitate reuse and
    ease of reading.

    >>> ls = Command("ls")
    >>> assert ls(__file__).run() == [__file__]
    >>> la = ls("-la")
    >>> result = la(__file__).run()
    >>> assert len(result) == 1
    >>> assert __file__ in result[0]
    >>> assert "-rw-r--r--" in result[0]

    You can the environmental variables for this command by using it as a
    `dict`, and change it's working directory by setting
    :attr:`working_directory`.
    """

    environ: Dict[str, str] = os.environ.copy()  #: :meta hide-value:
    """Environmental variables to execute this command with."""

    working_directory: Optional[File] = None
    """Directory to execute this command in."""

    file: File
    """File to execute."""

    arguments: List[Any]
    """Arguments to execute this command with."""

    def __init__(self, file: File, *arguments, **flags):
        """
        Create a new command.

        First all positional arguments are appended to the arguments list for
        this command. Then each keyword argument is interpreted as a flag
        using :func:`to_flag` and appended. This makes the Python call match
        the order of the resulting command. To add more positional arguments
        :meth:`call <__call__>` this command

        This means that the command `git --no-pager diff -M -- src`
        can to be constructed like so:

        >>> cmd = Command("git", no_pager=True)("diff", M=True)("--", "src")
        >>> str(cmd)
        'git --no-pager diff -M -- src'

        Here you can also see how keyword arguments are converted to flags,
        snake_case is converted to kebab-case and keywords longer then one
        character becomes long options, while single character ones become
        short options.

        :param file: path to the executable for this command
        :param arguments: arguments for this command
        :param flags: flags for this command
        """
        super().__init__()

        self.file = file
        self.arguments = list(arguments)
        self.arguments.extend(
            to_flag(KEYWORD_ARGUMENT_TO_FLAG_REGEXP.sub("-", name), value)
            for name, value in flags.items()
        )

    __getitem__ = delegate("environ")
    __setitem__ = delegate("environ")
    __delitem__ = delegate("environ")

    def __call__(self, *arguments, **flags):
        """Return a new :class:`command <Command>` with the given arguments and flags appended."""
        new = Command(self.file, *self.arguments, *arguments, **flags)
        new.environ = self.environ.copy()
        new.working_directory = self.working_directory
        return new

    def __sub__(self, other):
        """
        Return a new :class:`command <Command>` with the given flag appended.

        Allows for more ergonomic syntax
        >>> ls = Command("ls")
        >>> la = Flag("la", prefix="")
        >>> str(ls -la)
        'ls -la'
        """
        return self._append_flag(other, "-")

    def __add__(self, other):
        """
        Return a new :class:`command <Command>` with the given flag appended.

        Same as `__sub__` except for + flags.
        """
        return self._append_flag(other, "+")

    def _append_flag(self, flag: Union[Flag, str], prefix: flag.Prefix):
        if isinstance(flag, (Flag, str)):
            return self(to_flag(flag, prefix=prefix))
        else:
            return NotImplemented

    def __or__(self, command: Command):
        """Pipe the output from this command to the given one."""
        command < self
        self > PIPE
        return command

    def __and__(self, other: None) -> WaitableThread:
        """
        Run this command in the background/another thread.

        Due to limitations with Python syntax, this needs to be called with
        `None`. It returns a :class:`WaitableThread` for you to run, for
        example like this::

            with cmd & None: ...

        This makes it possible to support process substitution. For example
        you can write `diff <(left) <(right)` to print the diff between the
        output of `left` and `right`. how it works is that bash redirects
        `left` stdout to a fifo pipe special file, and similarly for `right`.
        Then it runs `diff` with the name of these fifo files, and finally
        cleans them up after `diff` exits.

        We can do the same. But we need to run each :class:`Popen` in a
        separate thread because :meth:`Popen.communicate` blocks until the
        command exits.
        """
        if other is not None:
            return NotImplemented
        else:
            return WaitableThread(target=self._run_in_background)

    def _run_in_background(self):
        """Helper for :meth:`Command.__and__`."""  # noqa: D401
        with self as process:
            with process:
                pass

    def __bool__(self):
        """Run this command and returns whether it existed with an non-zero exit code or not."""
        with self as process:
            return bool(process)

    def __iter__(self):
        """Iterate over the lines of the output of this command."""
        self > PIPE
        with self as process:
            yield from process

    def __str__(self) -> str:
        """
        Return this command as it would be called in a shell.

        The arguments are escaped using :func:`shlex.quote`, but your milage
        may vary.
        """
        arguments = " ".join(shlex.quote(str(arg)) for arg in self.arguments)
        return f"{self.file} {arguments}"

    @contextmanager
    def open(self, **kwargs):
        """
        Execute this command and automatically closes stdin/stdout/stderr.

        Yield the :class:`Popen` object for low-level access. Use
        :meth:`__enter__` for a better experience.
        """
        with self as process:
            yield process.popen

    def run(self):
        """Execute this command and return the outputted lines."""
        return list(self)

    @contextmanager
    def _context_manager(self):
        with self._temporary_directory() as tmp, ExitStack() as stack:
            arguments, kwargs = self._expand_arguments(tmp, stack)
            yield self._run(*arguments, **kwargs)

    def _temporary_directory(self) -> ContextManager[Optional[File]]:
        if any(isinstance(arg, Command) for arg in self.arguments):
            return TemporaryDirectory()
        else:
            # Since there are no command arguments, the temporary directory is
            # not needed. But to make mypy happy we return a dummy value.
            return nullcontext()

    def _expand_arguments(self, tmp: File, stack: ExitStack):
        background_counter = 0
        args: List[Any] = [self.file]
        kwargs = dict(
            stdin=stack.enter_context(self.stdin.open()),
            stdout=stack.enter_context(self.stdout.open()),
            stderr=stack.enter_context(self.stderr.open()),
        )

        for arg in self.arguments:
            if isinstance(arg, (Flag, Glob)):
                args.extend(arg)
            elif isinstance(arg, Command):
                background_counter += 1
                fifo = tmp / f"background-argument-{background_counter}"
                arg > fifo.mkfifo()
                args.append(stack.enter_context(arg & None))
            else:
                args.append(arg)

        return args, kwargs

    def _run(self, *arguments, stdin=None, stdout=None, stderr=None):
        return Process.run(
            *arguments,
            stdin=stdin,
            stdout=stdout,
            stderr=stderr,
            cwd=self.working_directory,
            environ=self.environ,
        )
