"""This module contains classes for working with files and file pattern."""
import abc
import os
from pathlib import Path
from typing import Iterator, Optional, cast


# Need to convince mypy this is fine.
# The metaclass magic is too confusing.
PathLike_register = cast(abc.ABCMeta, os.PathLike).register


@PathLike_register
class File:
    """
    Class to manipulate and execute files.

    File represents a filesystem path, and can be used to check this file
    and also to execute this file as a :meth:`python function <__call__>`.
    """

    path: Path

    def __init__(self, path):
        """
        Create a new :class:`File`.

        :param path: anything :class:`Path` accepts
        """
        self.path = Path(path)

    def __repr__(self):  # noqa D105
        return f'File("{self.path!s}")'

    def __str__(self):  # noqa D105
        return str(self.path)

    def __fspath__(self):
        """
        Magic method for :class:`os.PathLike`.

        Allows :class:`File` to be used anywhere a path is expected, like
        :func:`open`.
        """
        return self.path.__fspath__()

    def __bool__(self):
        """Whether this file exists."""
        return self.path.exists()

    def is_executable(self):
        """
        Whether the real uid/gid can execute this file.

        See :py:func:`os.access`
        """
        return os.access(self.path, os.X_OK)

    def mkfifo(
        self,
        mode: Optional[int] = None,
        directory: Optional[int] = None,
    ) -> "File":
        """Like :func:`~os.mkfifo`."""
        if isinstance(mode, int):
            os.mkfifo(self, mode=mode, dir_fd=directory)
        else:
            os.mkfifo(self, dir_fd=directory)

        return self

    def __eq__(self, other):
        """Equality magic method, semantics like :meth:`Path.samefile`."""
        if self is other:
            return True
        elif not isinstance(other, os.PathLike):
            return False

        if isinstance(other, File):
            other = other.path

        return self.path.samefile(other)

    def __len__(self):
        """Return the size of this file in bytes or number of entries in this directory."""
        if self.path.is_file():
            return self.path.stat().st_size
        else:
            return len(list(self))

    def __ge__(self, other):
        """
        Return self ≥ other by their :func:`length <len>`, or directly as an `int`.

        See :meth:`__len__`.
        """
        return len(self) >= self._len_or_int(other)

    def __gt__(self, other):
        """
        Return self > other by their :func:`length <len>`, or directly as an `int`.

        See :meth:`__len__`.
        """
        return len(self) > self._len_or_int(other)

    def __le__(self, other):
        """
        Return self < other by their :func:`length <len>`, or directly as an `int`.

        See :meth:`__len__`.
        """
        return len(self) <= self._len_or_int(other)

    def __lt__(self, other):
        """
        Return self ≥ other by their :func:`length <len>`, or directly as an `int`.

        See :meth:`__len__`.
        """
        return len(self) < self._len_or_int(other)

    def _len_or_int(self, value):
        try:
            return len(value)
        except TypeError:
            return int(value)

    def __call__(self, *arguments, **flags):
        """
        Prepare to execute this file using the given arguments and flags.

        See :class:`Command`
        """
        # Avoid circular imports
        from .command import Command

        return Command(self, *arguments, **flags)

    def __abs__(self):
        """
        Make the file path absolute.

        See :meth:`Path.resolve`
        """
        return File(self.path.resolve(strict=True))

    def __invert__(self):
        """Prefix and expand ~ like :meth:`Path.expanduser`."""
        path = str(self.path)
        if not path.startswith("~"):
            path = f"~{path}"

        return File(Path(path).expanduser())

    def __truediv__(self, part):
        """
        Append the given part to this file.

        >>> File("foo")/"bar/baz"
        File("foo/bar/baz")
        """
        return File(self.path / part)

    def __mul__(self, pattern):
        """
        Create a glob pattern from this file and the given pattern.

        See :meth:`Path.glob`
        """
        return Glob(self, f"*{pattern}")

    def __pow__(self, pattern):
        """
        Create a recursive glob pattern from this file and the given pattern.

        See :meth:`Path.glob`
        """
        return Glob(self, f"**{pattern}")

    def __iter__(self):
        """Iterate over the files in this directory, or lines in this file as appropriate."""
        if self.path.is_dir():
            for entry in self.path.iterdir():
                yield File(entry)
        else:
            yield from open(self, encoding="utf8").readlines()


class Glob(File):
    """
    Class that represents a file pattern, commonly known as a "glob".

    For example to list all Python source files under "src", you would write
    `Glob("src", "*.py")`.

    See :meth:`Path.glob` for available patterns.
    """

    pattern: str

    def __init__(self, path, pattern: str):  # noqa D105
        super().__init__(path)
        self.pattern = pattern

    def __repr__(self):  # noqa D105
        return f'Glob("{self.path!s}", {self.pattern!r})'

    def __str__(self):  # noqa D105
        return f"{self.path}{self.pattern}"

    def __truediv__(self, pattern):
        """Return a new :class:`Glob` with `"/"` and `pattern` appended."""
        return Glob(self, f"{self.pattern}/{pattern}")

    def __mul__(self, pattern):
        """Return a new :class:`Glob` with `"*"` and `pattern` appended."""
        return Glob(self, f"{self.pattern}*{pattern}")

    def __pow__(self, pattern):
        """Return a new :class:`Glob` with `"**"` and `pattern` appended."""
        return Glob(self, f"{self.pattern}**{pattern}")

    def __iter__(self) -> Iterator[File]:
        """Iterate over the matching files and directories."""
        for match in self.path.glob(self.pattern):
            yield File(match)
