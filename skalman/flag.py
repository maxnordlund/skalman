"""This module provides a class and function for handling command line flags."""
from re import fullmatch
import re
from typing import Any, Dict, Iterable, List, Optional, Tuple, Union, cast
from typing_extensions import Literal

Prefix = Union[Literal["-"], Literal["--"], Literal["+"]]


class Flag:
    """
    A class that represents a cli flag.

    Most cli's have flags, like `-v` or `--help`. This represents one of
    those, and through the clever use of special methods we can create a
    small <abbr title="Domain Specific Language">DSL</abbr> to make it more
    ergonomic.
    """

    prefix: Union[Prefix, Literal[""]]
    name: str
    value: Any

    def __init__(self, name: str, value: Any = True, prefix: Optional[Prefix] = None):
        """
        Create a flag.

        :param name: must be non-empty
        :param value: may be anything. Booleans are treated specially by :meth:`__iter__`.
        :param prefix: the `"--"` in `--help`, or the `"+"` in `+short`.
        """
        assert len(name) > 0

        self.name = name
        self.value = value
        self.prefix = prefix if prefix is not None else ""

    def __call__(self, value):
        """
        Return a new :class:`Flag` with the given value.

        >>> no_value = Flag("o", prefix="-")
        >>> with_value = no_value("some/path")
        >>> str(no_value)
        '-o'
        >>> str(with_value)
        '-o=some/path'
        """
        return Flag(self.name, value, self.prefix)

    def __neg__(self):
        """
        Return a new :class:`Flag` prefixed with `"-"`.

        >>> help = Flag("help")
        >>> str(--help)
        '--help'
        """
        return Flag(self.name, self.value, f"-{self.prefix}")

    def __pos__(self):
        """
        Return a new :class:`Flag` prefixed with `"+"`.

        >>> short = Flag("short")
        >>> str(+short)
        '+short'
        """
        return Flag(self.name, self.value, f"+{self.prefix}")

    def __sub__(self, other):
        """
        Concat this with another :class:`flag <Flag>` or :class:`str`.

        Only the second, rightmost, class:`flag <Flag>` may have a value.

        >>> no, pager = Flag("no", prefix="--"), Flag("pager")
        >>> str(no-pager)
        '--no-pager'
        """
        if isinstance(other, Flag):
            assert self.value is True, "Only the second flag may have a value"

            return Flag(f"{self.name}-{other.name}", other.value, self.prefix)
        else:
            return NotImplemented

    def __repr__(self):  # noqa D105
        return f"Flag({self.name!r}, {self.value!r}, {self.prefix!r})"

    def __str__(self):  # noqa D105
        return "=".join(str(part) for part in self)

    def __iter__(self):
        """
        Iterate over the parts this flags is composed over.

        First the prefix and name of this flag is yielded, then the value
        depending on its type.

        If the flag is `False` nothing is yielded, effectively disabling this
        flag.

        If this flags value is `True`, only the prefix and name is yielded.
        This matches common cli flag usage. For example in `git --help`,
        `help` is a boolean flag. However in `cut -d=" "`, `d` has the value
        `" "`.

        Otherwise the value is yielded. Note that no conversion to
        :class:`str` is done. That is up to the caller to decide.
        """
        if self.value is False:
            return

        yield f"{self.prefix}{self.name}"

        if self.value is not True:
            # Only non-boolean flags have a value to be emitted
            yield self.value


FLAG_EXTRACTION_REGEXP = re.compile(
    r"(?P<prefix>[+-]+)?(?P<name>[^=]+)(?:=(?P<value>.*))?"
)


def to_flag(
    name: Union[Flag, str],
    value: Any = True,
    prefix: Optional[Prefix] = None,
) -> Flag:
    """
    Return the given `name` as a :class:`Flag`, if it isn't one already.

    The `prefix` and `name` are combined and parsed, thus you can give this a
    flag as text. It also checks the length of the flag name to determine if
    it should be prefixed by one dash, for single character flags, or two,
    for all other. THis matches short/long options commonly found in cli's.

    >>> to_flag("--help")
    Flag('help', True, '--')
    >>> to_flag(to_flag("help", prefix="-"), prefix="-")
    Flag('help', True, '--')

    >>> to_flag("short", prefix="+")
    Flag('short', True, '+')

    >>> to_flag("k", "test_function", "--")
    Flag('k', 'test_function', '--')
    >>> to_flag("--k=test_function")
    Flag('k', 'test_function', '--')

    :param name: a :class:`flag <Flag>` or a str describing one
    :param value: optional value for the resulting :class:`flag <Flag>`
    :param prefix: optional prefix for the resulting :class:`flag <Flag>`
    """
    if isinstance(name, Flag):
        if prefix is None:
            pass
        elif prefix == "--":
            name = --name
        elif prefix == "-":
            name = -name
        elif prefix == "+":
            name = +name
        else:
            raise ValueError(f'unknown prefix "{prefix}", must be either "-" or "+"')

        if value is None:
            return cast(Flag, name)
        else:
            return cast(Flag, name)(value)
    elif name == "":
        raise ValueError("name must be non-empty")

    match = fullmatch(FLAG_EXTRACTION_REGEXP, f"{prefix or ''}{name}")
    assert match is not None, "can't happen because name is not the empty string"

    return Flag(*_normalize_prefix(value, **match.groupdict()))


def _normalize_prefix(
    default: Any,
    prefix: Optional[str] = None,
    value: Optional[str] = None,
    *,
    name: str,
) -> Tuple[str, Any, Prefix]:
    value = value if value is not None else default

    if prefix is not None:
        pass
    elif len(name) == 1:
        prefix = "-"
    else:
        prefix = "--"

    return name, value, cast(Prefix, prefix)


FlagDict = Dict[str, Union[bool, List[Any]]]


def into_dict(flags: Iterable[Flag]) -> FlagDict:
    """
    Group the given flags into a :class:`dict`.

    `False` flags are skipped, `True` flags are set to `True` and all other
    flags are appended to a list. Latter flags overwrite older ones.

    :param flags: tuple of flags to parse
    """
    mapping: FlagDict = dict()

    for flag in flags:
        if flag.value is False:
            continue
        elif flag.value is True:
            mapping[flag.name] = True
        else:
            values = mapping.get(flag.name)

            if isinstance(values, list):
                values.append(flag.value)
            else:
                mapping[flag.name] = [flag.value]

    return mapping
