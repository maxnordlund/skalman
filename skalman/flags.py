"""Magic module for conjuring command :class:`flags <skalman.flag.Flag>`."""
from .flag import Flag


# Must be a list of strings
__all__: list[str] = []


def __getattr__(flag) -> Flag:
    # Import into local scope to ensure _all_ flags are available,
    # even Flag itself
    from .flag import to_flag

    # Return just the bare flag, with no value or prefix
    return to_flag(flag)


# See comment in __getattr__
del Flag
