"""This module contains some general helpers for skalman."""
from __future__ import annotations

from contextlib import contextmanager
from threading import Thread
from typing import (
    TYPE_CHECKING,
    Callable,
    Generic,
    Iterator,
    Optional,
    Type,
    TypeVar,
    overload,
)

if TYPE_CHECKING:
    from typing_extensions import ContextManager


T = TypeVar("T")
V = TypeVar("V")


class delegate:
    """
    Delegate to the similarly named property of the given field.

    This means ``bar = delegate("field")`` works the same as::

        class Spam:
            @property
            def bar(self):
                return self.field.bar

    Here's another example:

    >>> class Thru:
    ...     pop = delegate("dictionary")
    ...
    ...     def __init__(self):
    ...         self.dictionary = { "number": 123 }
    ...
    >>> thru = Thru()
    >>> assert thru.pop("number") == 123
    >>> assert thru.pop("number")
    Traceback (most recent call last):
        ...
    KeyError: 'number'
    """

    name: Optional[str]
    """The field this delegate is assigned to."""

    field: str
    """The field to delegate to."""

    def __init__(self, field: str, name: Optional[str] = None):  # noqa D105
        self.field = field
        self.name = name

    def __get__(self, instance, owner):  # noqa D105
        return getattr(self._get(instance), self.name)

    def __set__(self, instance, value):  # noqa D105
        setattr(self._get(instance), self.name, value)

    def ___delete__(self, instance):  # noqa D105
        delattr(self._get(instance), self.name)

    def __set_name__(self, owner, name):  # noqa D105
        if self.name is None:
            self.name = name

    def _get(self, instance):
        return getattr(instance, self.field)


# https://github.com/bottlepy/bottle/commit/fa7733e075da0d790d809aa3d2f53071897e6f76
# https://github.com/pydanny/cached-property/blob/master/cached_property.py
class cached_property(Generic[T]):
    """
    A memoized property decorator.

    A property that is only computed once per instance and then replaces itself
    with an ordinary attribute. Deleting the attribute resets the property.
    """

    def __init__(self, func: Callable[[V], T]):  # noqa D105
        self.__doc__ = func.__doc__
        self.func = func

    @overload
    def __get__(self, obj: None, cls: Type[T]) -> cached_property[T]:
        ...

    @overload
    def __get__(self, obj: V, cls: Type[T]) -> T:
        ...

    def __get__(self, obj, cls):  # noqa D105
        if obj is None:
            return self
        value = obj.__dict__[self.func.__name__] = self.func(obj)
        return value


class ContextManagerMixin(Generic[T]):
    """
    Convenience class for writing context mangers that use context managers.

    It turns out to bit quite hard to properly implement __enter__ and
    __exit__ over using :func:`contextlib.contextmanager`. Especially with
    nested context managers.

    First off you need to call __enter__ and __exit__ yourself on the context
    manager (since using the ``with`` statement would call __exit__ too
    soon). Then you need to make sure to handle exceptions raised by their
    __exit__ inside your __exit__.

    That's easy enough, until you start to use multiple context managers,
    which means you need to take care to call __exit__ on all of them, even
    if some raised an exception in either __enter__ or __exit__.

    On the other hand, using :func:`~contextlib.contextmanager` is much easier::

        @contextmanager
        def wrap(input, output):
            with open(input) as i, open(output) as o:
                yield i, o

    Your caller can be sure ``i`` and ``o`` are opened during their call, and
    also that both will be closed correctly.

    >>> class CM(ContextManagerMixin):
    ...     def __ini__(self, input, output):
    ...         self.input = input
    ...         self.output = output
    ...
    ...     @contextmanager
    ...     def _context_manager(self):
    ...         with open(self.input) as i, open(self.output) as o:
    ...             yield i, o
    ...

    This is a trivial example, but you can see how in the real world this can
    be useful when you already have a class and want to augment it into a
    proper context manager.

    The subclass must implement :meth:`~._context_manager`. In return this
    mixin will take care of implementing __enter__ and __exit__ correctly.
    """

    __context: ContextManager[T]

    # D401: First line should be in imperative mood
    # But in this case it doesn't make sense. _This_ method does nothing,
    # it's for the subclass to implement.
    # But that does read as nice to me, so disable it.
    @contextmanager
    def _context_manager(self) -> Iterator[T]:
        """
        Hook implemented by subclasses to do the actual work.

        This must return a context manager, easiest is to use
        :meth:`~._context_manager`.
        """  # noqa: D401
        raise NotImplementedError("Subclasses must implement this")

    def __enter__(self) -> T:  # noqa D105
        self.__context = self._context_manager()
        assert callable(self.__context.__enter__) and callable(self.__context.__exit__)
        return self.__context.__enter__()

    def __exit__(self, exc_type, exc_value, traceback):  # noqa D105
        return self.__context.__exit__(exc_type, exc_value, traceback)


class WaitableThread(Thread):
    """
    Thread you can wait on using a with statement.

    The thread is started upon entering the context manager, and is joined
    when exiting. The effect is that is running inside the with statement and
    guaranteed to have stopped by the time you exit.

    >>> with WaitableThread(target=print, args=("hello",)):
    ...     pass
    hello
    """

    def __enter__(self) -> None:  # noqa D105
        self.start()

    def __exit__(self, exc_type, exc_value, traceback):  # noqa D105
        self.join()
