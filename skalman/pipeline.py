"""
This contains the pipeline builder class.

It's the pure part of :mod:`skalman`.

>>> pipeline = File("ls")() | File("grep")(v=True)("hello")
>>> print(str(pipeline))
ls | grep -v hello
>>> print(repr(pipeline))
Executable(File("ls")) | Executable(File("grep"), Flag('v', True, '-'), 'hello')
"""
from __future__ import annotations

import abc
import asyncio
import contextlib
import io
import os
import re
import shlex
import shutil
import sys
from asyncio import subprocess
from enum import Enum
from io import BytesIO, StringIO, TextIOWrapper
from os import PathLike
from pathlib import Path
from typing import (
    IO,
    TYPE_CHECKING,
    Any,
    BinaryIO,
    Callable,
    Dict,
    Generic,
    Iterator,
    List,
    Optional,
    TextIO,
    TypeVar,
    Union,
    cast,
    overload,
)

from typing_extensions import Protocol, runtime_checkable

from .flag import Flag, Prefix, to_flag
from .helpers import delegate

if TYPE_CHECKING:
    from _typeshed import OpenBinaryMode, OpenTextMode
    from typing_extensions import ContextManager

    SubprocessIO = Union[int, IO, None]

T = TypeVar("T")

KEYWORD_ARGUMENT_TO_FLAG_REGEXP = re.compile(r"(?<!\A)_")


class IOType(Enum):
    """
    Type of IO for :class:`Redirect`.

    Can be either text of binary. See :func:`open` for more information.
    """

    BINARY = "b"
    TEXT = "t"


class IOMode(Enum):
    """
    IO mode for :class:`Redirect`.

    Can be one of read, write, or append. See :func:`open` for more
    information.
    """

    READ = "r"
    WRITE = "w"
    APPEND = "a"


@runtime_checkable
class SupportsOpen(Protocol):
    """
    Protocol for :meth:`IO.open` like methods.

    See :func:`open`
    """

    @overload
    def open(self, mode: OpenBinaryMode) -> BinaryIO:
        ...

    @overload
    def open(
        self, mode: OpenTextMode, encoding: str, newline: Optional[str]
    ) -> TextIOWrapper:
        ...


class Segment:
    """
    This represents a segment of a shell pipeline.

    You can think of it as the part between pipes, `|`.

    The instances are immutable to allow reuse and easily building complex
    pipelines incrementally.
    """

    command: Any
    """
    Command to run.

    Can be a file to execute, or a python callable.
    """

    previous: Optional[Segment]
    """The :class:`segment <Segment>`/:attr:`command` before this one in the pipeline."""

    io: Dict[int, Redirect]
    """Input/Outputs for the :attr:`command`."""

    def __init__(
        self,
        command: Any,
        previous: Optional[Segment] = None,
        file_descriptors: Optional[List[Any]] = None,
        stdin=None,
        stdout=None,
        stderr=None,
    ):
        """
        Create and return a new pipeline segment.

        :param command: the command to run
        :param previous: the previous segment of this pipeline, if any
        :param file_descriptors: list of additional file descriptors for this segment
        :param stdin: file descriptor that represents this segment's input
        :param stdout: file descriptor that represents this segment's normal output
        :param stderr: file descriptor that represents this segment's error output
        """
        if isinstance(command, (str, PathLike)):
            self.command = Executable.which(command)
        else:
            self.command = command

        self.previous = previous
        self.io = dict()

        if previous is None:
            self._set_io(0, stdin, sys.stdin, IOMode.READ)
        else:
            assert stdin is None, "can't both pipe and redirect stdin"
            self.io[0] = Redirect(self, 0, subprocess.PIPE, IOMode.READ, text=False)

        self._set_io(1, stdout, sys.stdout, IOMode.WRITE)
        self._set_io(2, stderr, sys.stderr, IOMode.WRITE)

        if file_descriptors is not None:
            for fileno, descriptor in enumerate(file_descriptors, 3):
                self._set_io(fileno, descriptor, os.devnull, IOMode.WRITE)

    def _set_io(self, fileno: int, source: Any, default: Any, mode: IOMode):
        if not isinstance(source, Redirect):
            source = Redirect(
                self,
                fileno=fileno,
                target=source if source is not None else default,
                mode=mode,
                text=True,
            )

        self.io[fileno] = source

    @property
    def stdin(self) -> Redirect[TextIO]:
        """Return the :class:`Redirect <file descriptor>` that represents stdin."""
        return self.io[0]

    @property
    def stdout(self) -> Redirect[TextIO]:
        """Return the :class:`Redirect <file descriptor>` that represents stdout."""
        return self.io[1]

    @property
    def stderr(self) -> Redirect[TextIO]:
        """Return the :class:`Redirect <file descriptor>` that represents stderr."""
        return self.io[2]

    def __str__(self) -> str:  # noqa D105
        return self._to_printable(str)

    def __repr__(self) -> str:  # noqa D105
        return self._to_printable(repr)

    def _to_printable(self, to_string: Callable[[Any], str]):
        parts: List[str] = []

        if self.previous is not None:
            parts.append(to_string(self.previous))
            parts.append("|")

        parts.append(to_string(self.command))

        for redirect in self.io.values():
            formatted = str(redirect)
            if formatted:
                parts.append(formatted)

        return " ".join(parts)

    def __lt__(self, stdin):
        """
        Redirect this segment's stdin.

        >>> target = Segment(File("some-command"))
        >>> result = target < "input_file"
        >>> result.stdin.target
        'input_file'
        >>> result.stdin.mode
        <IOMode.READ: 'r'>
        """
        return (self & 0) < stdin

    def __gt__(self, stdout):
        """
        Redirect this segment's stdout, overwriting the target.

        >>> target = Segment(File("some-command"))
        >>> result = target > "output"
        >>> result.stdout.target
        'output'
        >>> result.stdout.mode
        <IOMode.WRITE: 'w'>
        """
        return (self & 1) > stdout

    def __rshift__(self, stdout):
        """
        Redirect this segment's stdout, appending the target.

        >>> target = Segment(File("some-command"))
        >>> result = target >> "app.log"
        >>> result.stdout.target
        'app.log'
        >>> result.stdout.mode
        <IOMode.APPEND: 'a'>
        """
        return (self & 1) >> stdout

    def __lshift__(self, source: Union[str, bytes]):
        """
        Redirect the given `str` or `bytes` to this segment's stdin.

        >>> target = Segment(File("some-command"))
        >>> result = target << "input"
        >>> result.stdin.target
        <_io.StringIO ...>
        >>> result.stdin.target.read()
        'input'
        """
        return (self & 0) < self._raw_input(source)

    def __rrshift__(self, source: Union[str, bytes]):
        """
        Redirect the given `str` or `bytes` to this segment's stdin.

        >>> target = Segment(File("some-command"))
        >>> result = b"input" >> target
        >>> result.stdin.target
        <_io.BytesIO ...>
        >>> result.stdin.target.read()
        b'input'
        """
        return (self & 0) < self._raw_input(source)

    @overload
    def _raw_input(self, source: str) -> StringIO:
        ...

    @overload
    def _raw_input(self, source: bytes) -> BytesIO:
        ...

    def _raw_input(self, source: Union[str, bytes]):
        if isinstance(source, str):
            return io.StringIO(source)
        else:
            return io.BytesIO(source)

    def __xor__(self, stderr):
        """
        Redirect this segment's stderr.

        This is inspired by `Fish 2 shell syntax`_ [#]_, as the normal
        ``2>error.log`` is not possible in python.

        >>> source = Segment(File("some-command"))
        >>> result = source ^ "error.log"
        >>> result.stderr.target
        'error.log'
        >>> result.stderr.mode
        <IOMode.WRITE: 'w'>

        .. _Fish 2 shell syntax: https://fishshell.com/docs/2.7/#redirects
        .. [#] Archived copy https://web.archive.org/web/20200118164052/https://fishshell.com/docs/2.7/
        """
        return (self & 2) > stderr

    def __and__(self, fileno: int):
        """Return the :class:`Redirect <file descriptor>` for the given file number."""
        if fileno in self.io:
            return self.io[fileno]
        else:
            return Redirect(self, fileno=fileno, target=os.devnull, mode=IOMode.WRITE)

    def __or__(self, command: Any):
        """Pipe the output from this segment into the input of the given one."""
        return Segment(command, previous=self)


class Redirect(Generic[T]):
    """
    This class represents a redirection of a :class:`Segment` IO.

    Mostly used to redirect standard in, out and/or err.
    """

    segment: Segment
    """Parent pipeline segment."""

    fileno: int
    """The file descriptor this redirects."""

    target: Union[T, SubprocessIO]
    """Target for this redirect."""

    type: IOType
    """
    File type (text or binary) to open the :attr:`~.source` with.

    See :func:`open` for details.
    """

    mode: IOMode
    """
    File mode (read, write or append) to open the :attr:`~.source` with.

    See :func:`open` for details.
    """

    def __init__(
        self, segment: Segment, fileno: int, target: T, mode: IOMode, text=False
    ):  # noqa D105
        if str(target) == os.devnull:
            self.target = subprocess.DEVNULL
        else:
            self.target = target

        self.type = IOType.TEXT if text else IOType.BINARY
        self.mode = mode
        self.segment = segment
        self.fileno = fileno

    def __str__(self) -> str:  # noqa D105
        if self.fileno == 0:
            if self.target in [0, sys.stdin, subprocess.PIPE]:
                return ""
            else:
                return f"<{self.target}"
        elif self.fileno == 1:
            if self.target in [1, sys.stdout, subprocess.STDOUT]:
                return ""
            elif self.mode == IOMode.APPEND:
                return f">>{self.target}"
            else:
                return f">{self.target}"
        elif self.fileno == 2:
            if self.target in [2, sys.stderr]:
                return ""
            else:
                return f"^{self.target}"
        elif self.mode == IOMode.APPEND:
            return f"&{self.fileno}>>{self.target}"
        else:
            return f"&{self.fileno}>{self.target}"

    def open(self) -> ContextManager[SubprocessIO]:
        """Open :attr:`source` according to :attr:`mode` and return the result."""
        source = self.target

        if source in (sys.stdin, sys.stdout, sys.stderr):
            # subprocess treats None as inherit this process stdio
            return contextlib.nullcontext()
        elif isinstance(source, int):
            # One of the subprocess constants or raw file descriptor
            return contextlib.nullcontext(source)
        elif isinstance(source, io.IOBase):
            # Already opened file/StringIO
            source = cast(IO, source)
            return contextlib.nullcontext(source)
        elif isinstance(source, (PathLike, str)):
            return contextlib.nullcontext(open(source, **self._open_flags()))
        elif isinstance(source, SupportsOpen):
            return source.open(**self._open_flags())
        else:
            raise TypeError(f"{type(source).__name__!r} not openable")

    def _open_flags(self):
        mode = self.type.value + self.mode.value
        if self.type == IOType.BINARY:
            return dict(mode=mode)
        else:
            return dict(mode=mode, encoding="utf8", newline=None)

    def __lt__(self, input):  # noqa D105
        return self._redirect(input, IOMode.READ)

    def __gt__(self, output):  # noqa D105
        return self._redirect(output, IOMode.WRITE)

    def __rshift__(self, output):  # noqa D105
        return self._redirect(output, IOMode.APPEND)

    def _redirect(self, io, mode: IOMode) -> Segment:
        new = type(self.segment)(self.segment.command)
        new.io = self.segment.io.copy()

        if isinstance(io, Redirect):
            assert io.mode == mode, "must redirect using the same mode"
            new.io[self.fileno] = Redirect(new, self.fileno, io.target, io.mode)
        else:
            new.io[self.fileno] = Redirect(new, self.fileno, io, mode)
        return new


# Need to convince mypy this is fine.
# The metaclass magic is too confusing.
PathLike_register = cast(abc.ABCMeta, os.PathLike).register


@PathLike_register
class File:
    """
    Class to manipulate and execute files.

    File represents a filesystem path, and can be used to check this file
    and also to execute this file as a :meth:`python function <__call__>`.
    """

    path: Path

    def __init__(self, path):
        """
        Create a new :class:`File`.

        :param path: anything :class:`pathlib.Path` accepts
        """
        self.path = Path(path)

    @property
    def name(self) -> str:
        return self.path.name

    def __repr__(self):  # noqa D105
        return f'File("{self.path!s}")'

    def __str__(self):  # noqa D105
        return str(self.path)

    def __fspath__(self):
        """
        Magic method for :class:`os.PathLike`.

        Allows :class:`File` to be used anywhere a path is expected, like
        :func:`open`.
        """
        return self.path.__fspath__()

    def __bool__(self):
        """Whether this file exists."""
        return self.path.exists()

    def is_executable(self):
        """
        Whether the real uid/gid can execute this file.

        See :func:`os.access`
        """
        return os.access(self.path, os.X_OK)

    def mkfifo(
        self,
        mode: Optional[int] = None,
        directory: Optional[int] = None,
    ) -> "File":
        """Like :func:`~os.mkfifo`."""
        if isinstance(mode, int):
            os.mkfifo(self, mode=mode, dir_fd=directory)
        else:
            os.mkfifo(self, dir_fd=directory)

        return self

    def __eq__(self, other):
        """Equality magic method, semantics like :meth:`pathlib.Path.samefile`."""
        if self is other:
            return True
        elif not isinstance(other, os.PathLike):
            return False

        if isinstance(other, File):
            other = other.path

        return self.path.samefile(other)

    def __len__(self):
        """Return the size of this file in bytes or number of entries in this directory."""
        if self.path.is_file():
            return self.path.stat().st_size
        else:
            return sum(1 for _ in self.path.iterdir())

    def __ge__(self, other):
        """
        Return self ≥ other by their :func:`length <len>`, or directly as an `int`.

        See :meth:`__len__`.
        """
        return len(self) >= self._len_or_int(other)

    def __gt__(self, other):
        """
        Return self > other by their :func:`length <len>`, or directly as an `int`.

        See :meth:`__len__`.
        """
        return len(self) > self._len_or_int(other)

    def __le__(self, other):
        """
        Return self < other by their :func:`length <len>`, or directly as an `int`.

        See :meth:`__len__`.
        """
        return len(self) <= self._len_or_int(other)

    def __lt__(self, other):
        """
        Return self ≥ other by their :func:`length <len>`, or directly as an `int`.

        See :meth:`__len__`.
        """
        return len(self) < self._len_or_int(other)

    def _len_or_int(self, value):
        try:
            return len(value)
        except TypeError:
            return int(value)

    def __abs__(self):
        """
        Make the file path absolute.

        See :meth:`pathlib.Path.resolve`
        """
        return File(self.path.resolve(strict=True))

    def __invert__(self):
        """Prefix and expand ~ like :meth:`pathlib.Path.expanduser`."""
        path = str(self.path)
        if not path.startswith("~"):
            path = f"~{path}"

        return File(Path(path).expanduser())

    def __truediv__(self, part):
        """
        Append the given part to this file.

        >>> File("foo")/"bar/baz"
        File("foo/bar/baz")
        """
        return File(self.path / part)

    def __mul__(self, pattern):
        """
        Create a glob pattern from this file and the given pattern.

        See :meth:`pathlib.Path.glob`
        """
        return Glob(self, f"*{pattern}")

    def __pow__(self, pattern):
        """
        Create a recursive glob pattern from this file and the given pattern.

        See :meth:`pathlib.Path.glob`
        """
        return Glob(self, f"**{pattern}")

    def __iter__(self):
        """Iterate over the files in this directory, or lines in this file as appropriate."""
        if self.path.is_dir():
            for entry in self.path.iterdir():
                yield File(entry)
        else:
            yield from open(self, encoding="utf8").readlines()

    def __call__(self, *arguments, **flags):
        """
        Prepare to execute this file using the given arguments and flags.

        See :class:`Executable`
        """
        return Executable(self, *arguments, **flags)


class Glob(File):
    """
    Class that represents a file pattern, commonly known as a "glob".

    For example to list all Python source files under "src", you would write
    `Glob("src", "*.py")`.

    See :meth:`pathlib.Path.glob` for available patterns.
    """

    pattern: str

    def __init__(self, path, pattern: str):  # noqa D105
        super().__init__(path)
        self.pattern = pattern

    def __repr__(self):  # noqa D105
        return f'Glob("{self.path!s}", {self.pattern!r})'

    def __str__(self):  # noqa D105
        return f"{self.path}{'/' if self.path.is_dir() else ''}{self.pattern}"

    def __abs__(self):
        """
        Make the file path absolute.

        See :meth:`pathlib.Path.resolve`
        """
        return Glob(self.path.resolve(strict=True), self.pattern)

    def __truediv__(self, pattern):
        """Return a new :class:`Glob` with `"/"` and `pattern` appended."""
        return Glob(self, f"{self.pattern}/{pattern}")

    def __mul__(self, pattern):
        """Return a new :class:`Glob` with `"*"` and `pattern` appended."""
        return Glob(self, f"{self.pattern}*{pattern}")

    def __pow__(self, pattern):
        """Return a new :class:`Glob` with `"**"` and `pattern` appended."""
        return Glob(self, f"{self.pattern}**{pattern}")

    def __iter__(self) -> Iterator[File]:
        """Iterate over the matching files and directories."""
        for match in self.path.glob(self.pattern):
            yield File(match)

    def __len__(self):
        """Return the number of matches for this glob."""
        return sum(1 for _ in self.path.glob(self.pattern))


class Executable(Segment):
    """
    Class that represents an executable program.

    This also acts as a builder for such programs to facilitate reuse and
    ease of reading.

    >>> ls = Executable("ls")
    >>> assert (ls(__file__).run()) == [__file__]
    >>> la = ls("-la")
    >>> result = la(__file__).run()
    >>> assert len(result) == 1
    >>> assert __file__ in result[0]
    >>> assert "-rw-r--r--" in result[0]

    You can the environmental variables for this command by using it as a
    `dict`, and change it's working directory by setting
    :attr:`working_directory`.
    """

    environ: Dict[str, str]
    """Environmental variables to execute this command with."""

    working_directory: Optional[File] = None
    """Directory to execute this command in."""

    file: File
    """File to execute."""

    arguments: List[Any]
    """Arguments to execute this command with."""

    @staticmethod
    def which(command: Union[PathLike, str]) -> Optional[Executable]:
        """Lookup the given `bin` in PATH, and if found, return it as a :class:`executable <Executable>`."""
        path = shutil.which(command)
        if path is None:
            return None
        else:
            return File(path)()

    def __init__(self, file: File, *arguments, **flags) -> None:
        """
        Create and return a new executable.

        First all positional arguments are appended to the arguments list for
        this command. Then each keyword argument is interpreted as a flag
        using :func:`to_flag` and appended. This makes the Python call match
        the order of the resulting command. To add more positional arguments
        :meth:`call <__call__>` this command

        This means that the command `git --no-pager diff -M -- src`
        can to be constructed like so:

        >>> cmd = Executable("git", no_pager=True)("diff", M=True)("--", "src")
        >>> str(cmd)
        'git --no-pager diff -M -- src'

        Here you can also see how keyword arguments are converted to flags,
        snake_case is converted to kebab-case and keywords longer then one
        character becomes long options, while single character ones become
        short options.

        :param file: path to the executable for this command
        :param arguments: arguments for this command
        :param flags: flags for this command
        """
        self.environ = os.environ.copy()
        self.file = file
        self.arguments = list(arguments)
        self.arguments.extend(
            to_flag(KEYWORD_ARGUMENT_TO_FLAG_REGEXP.sub("-", name), value)
            for name, value in flags.items()
        )

    __getitem__ = delegate("environ")
    __setitem__ = delegate("environ")
    __delitem__ = delegate("environ")

    def __str__(self) -> str:
        """
        Return this command as it would be called in a shell.

        The arguments are escaped using :func:`shlex.quote`, but your milage
        may vary.

        >>> command = Executable(File("grep"))
        >>> str(command)
        'grep'
        >>> command = command(v=True)
        >>> str(command)
        'grep -v'
        >>> command = command("hello", o=True)
        >>> str(command)
        'grep -v hello -o'
        """
        if len(self.arguments):
            arguments = " ".join(shlex.quote(str(arg)) for arg in self.arguments)
            return f"{self.file} {arguments}"
        else:
            return str(self.file)

    def __repr__(self) -> str:  # noqa D105
        arguments = [repr(self.file)]
        arguments.extend(map(repr, self.arguments))
        return f"Executable({', '.join(arguments)})"

    def __call__(self, *arguments, **flags) -> Executable:
        """Return a new :class:`executable <Executable>` with the given arguments and flags appended."""
        new = Executable(self.file, *self.arguments, *arguments, **flags)
        new.environ = self.environ.copy()
        new.working_directory = self.working_directory
        return new

    def __sub__(self, other):
        """
        Return a new :class:`executable <Executable>` with the given flag appended.

        Allows for more ergonomic syntax

        >>> ls = Executable("ls")
        >>> la = Flag("la", prefix="")
        >>> str(ls -la)
        'ls -la'
        """
        return self._append_flag(other, "-")

    def __add__(self, other):
        """
        Return a new :class:`executable <Executable>` with the given flag appended.

        Same as `__sub__` except for + flags.

        >>> dig = Executable("dig")
        >>> short = Flag("short", prefix="")
        >>> str(dig +short)
        'dig +short'
        """
        return self._append_flag(other, "+")

    def _append_flag(self, flag: Union[Flag, str], prefix: Prefix):
        if isinstance(flag, (Flag, str)):
            return self(to_flag(flag, prefix=prefix))
        else:
            return NotImplemented

    def run(self):
        return asyncio.get_event_loop().run_until_complete(self)

    def __await__(self):  # noqa D105
        return self._run_async()

    async def _run_async(self):
        subprocess.create_subprocess_exec()
