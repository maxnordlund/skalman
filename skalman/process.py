from __future__ import annotations

from contextlib import contextmanager
from subprocess import CalledProcessError, Popen
from typing import IO, Tuple

from skalman.helpers import ContextManagerMixin, cached_property


class Process(ContextManagerMixin[Tuple[IO, IO, int]]):
    """
    Class representing a running :class`command <Command>`.

    This is mostly for internal use, and shouldn't be directly constructed.
    However, the :class:`cached properties <cached_property>` are for public
    consumption.

    These properties all block until the command has finished running.
    """

    @classmethod
    def run(
        cls,
        *arguments,
        stdin=None,
        stdout=None,
        stderr=None,
        cwd=None,
        environ=None,
    ):
        """
        Create a new Process from the given arguments.

        This is an convenience method for creating instances for this class as
        its constructor requires a :class:`Popen` instance.
        """
        return cls(
            Popen(
                arguments,
                bufsize=1,
                text=True,
                encoding="utf8",
                stdin=stdin,
                stdout=stdout,
                stderr=stderr,
                cwd=cwd,
                env=environ,
            )
        )

    def __init__(self, popen: Popen):  # noqa D105
        self.popen = popen

    @contextmanager
    def _context_manager(self):
        with self.popen as popen:
            stdout, stderr = popen.communicate()

            yield stdout, stderr, popen.returncode

    @cached_property
    def stdout(self):
        """Return the standard out for this process."""
        with self as (stdout, _, _):
            return stdout

    @cached_property
    def stderr(self):
        """Return the standard error for this process."""
        with self as (_, stderr, _):
            return stderr

    @cached_property
    def __int__(self):
        """Return the exit code for this process."""
        with self as (_, _, returncode):
            return returncode

    @cached_property
    def __bool__(self):
        """Return whether this existed with an non-zero exit code or not."""
        return int(self) == 0

    def __iter__(self):
        """
        Iterate over standard out for this process.

        If the process exited with an non-zero status, this raises
        :class:`CalledProcessError`.
        """
        with self as (stdout, stderr, returncode):
            if returncode != 0:
                raise CalledProcessError(
                    returncode,
                    " ".join(self.popen.args),
                    output=stdout,
                    stderr=stderr,
                )

            if stdout is not None:
                yield from stdout.splitlines()
