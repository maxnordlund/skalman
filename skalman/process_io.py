from __future__ import annotations

import sys

from .redirect import IOMode, Redirect


class ProcessIO:
    """
    This class represents a process' three standard IO devices, in, out and err.

    These can be redirected, see :class:`Redirect`.
    """

    stdin: Redirect
    stdout: Redirect
    stderr: Redirect

    def __init__(self):
        self.stdin = Redirect(sys.stdin, IOMode.READ)
        self.stdout = Redirect(sys.stdout, IOMode.WRITE)
        self.stderr = Redirect(sys.stderr, IOMode.WRITE)

    def __lt__(self, stdin):
        """
        Redirect this process stdin.

        >>> source = ProcessIO()
        >>> result = source < "input_file"
        >>> result.stdin.source
        'input_file'
        >>> result.stdin.mode
        <IOMode.READ: 'r'>
        """
        self.stdin = Redirect(stdin, IOMode.READ)
        return self

    def __gt__(self, stdout):
        """
        Redirect this process output, overwriting the target.

        >>> source = ProcessIO()
        >>> result = source > "output"
        >>> result.stdout.source
        'output'
        >>> result.stdout.mode
        <IOMode.WRITE: 'w'>
        """
        self.stdout = Redirect(stdout, IOMode.WRITE)
        return self

    def __rshift__(self, stdout):
        """
        Redirect this process output, appending the target.

        >>> source = ProcessIO()
        >>> result = source >> "app.log"
        >>> result.stdout.source
        'app.log'
        >>> result.stdout.mode
        <IOMode.APPEND: 'a'>
        """
        self.stdout = Redirect(stdout, IOMode.APPEND)
        return self

    def __xor__(self, stderr):
        """
        Redirect this process stderr.

        This is inspired by `Fish 2 shell syntax`_ [#]_, as the normal
        ``2>error.log`` is not possible in python.

        >>> source = ProcessIO()
        >>> result = source ^ "error.log"
        >>> result.stderr.source
        'error.log'
        >>> result.stderr.mode
        <IOMode.WRITE: 'w'>

        .. _Fish 2 shell syntax: https://fishshell.com/docs/2.7/#redirects
        .. [#] Archived copy https://web.archive.org/web/20200118164052/https://fishshell.com/docs/2.7/
        """
        self.stderr = Redirect(stderr, IOMode.WRITE)
        return self
