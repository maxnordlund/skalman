"""
This module contains classes related to process redirection.

Basically the `>`/`<`/`|` in your favorite shell.
"""
from __future__ import annotations

import contextlib
import io
import os
import subprocess
import sys
from enum import Enum
from io import TextIOWrapper
from os import PathLike
from typing import IO, TYPE_CHECKING, Any, BinaryIO, Optional, Union, cast, overload

from typing_extensions import Protocol, runtime_checkable

if TYPE_CHECKING:
    from _typeshed import OpenBinaryMode, OpenTextMode
    from typing_extensions import ContextManager

    SubprocessIO = Union[int, IO, None]


class IOType(Enum):
    """
    Type of IO for :class:`Redirect`.

    Can be either text of binary. See :func:`open` for more information.
    """

    BINARY = "b"
    TEXT = "t"


class IOMode(Enum):
    """
    IO mode for :class:`Redirect`.

    Can be one of read, write, or append. See :func:`open` for more
    information.
    """

    READ = "r"
    WRITE = "w"
    APPEND = "a"


@runtime_checkable
class SupportsOpen(Protocol):
    """
    Protocol for :meth:`IO.open` like methods.

    See :func:`open`
    """

    @overload
    def open(self, mode: OpenBinaryMode) -> BinaryIO:
        ...

    @overload
    def open(
        self, mode: OpenTextMode, encoding: str, newline: Optional[str]
    ) -> TextIOWrapper:
        ...


class Redirect:
    """This class represents a redirection of standard in, out or err."""

    source: Any
    """Input for this redirect."""

    type: IOType
    """
    File type (text or binary) to open the :attr:`~.source` with.

    See :func:`open` for details.
    """

    mode: IOMode
    """
    File mode (read, write or append) to open the :attr:`~.source` with.

    See :func:`open` for details.
    """

    def __init__(self, source: Any, mode: IOMode, text=False):  # noqa D105
        if str(source) == os.devnull:
            self.source = subprocess.DEVNULL
        else:
            self.source = source

        self.type = IOType.TEXT if text else IOType.BINARY
        self.mode = mode

    def open(self) -> ContextManager[SubprocessIO]:
        """Open :attr:`source` according to :attr:`mode` and return the result."""
        source = self.source

        if source in (sys.stdin, sys.stdout, sys.stderr):
            # subprocess treats None as inherit this process stdio
            return contextlib.nullcontext()
        elif isinstance(source, int):
            # One of the subprocess constants or raw file descriptor
            return contextlib.nullcontext(source)
        elif isinstance(source, io.IOBase):
            # Already opened file/StringIO
            source = cast(IO, source)
            return contextlib.nullcontext(source)
        elif isinstance(source, (PathLike, str)):
            return contextlib.nullcontext(open(source, **self._open_flags()))
        elif isinstance(source, SupportsOpen):
            return source.open(**self._open_flags())
        else:
            raise TypeError(f"{type(source).__name__!r} not openable")

    def _open_flags(self):
        mode = self.type.value + self.mode.value
        if self.type == IOType.BINARY:
            return dict(mode=mode)
        else:
            return dict(mode=mode, encoding="utf8", newline=None)
