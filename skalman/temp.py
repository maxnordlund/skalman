"""This module contains classes for creating temporary files and directories."""
import tempfile
from typing import Union

from .file import File
from .helpers import delegate


class TemporaryFile:
    """
    Create an return a temporary :class:`file <File>`.

    A combination of :class:`~tempfile.NamedTemporaryFile` and
    :class:`~tempfile.SpooledTemporaryFile`. The latter is used if you provide
    a `max_size` argument.
    """

    _file: Union[tempfile.SpooledTemporaryFile, tempfile._TemporaryFileWrapper]

    def __init__(
        self,
        mode="w+b",
        max_size: Union[int, None] = None,
        buffering=-1,
        encoding=None,
        newline=None,
        prefix=None,
        suffix=None,
        dir=None,
    ) -> None:
        if max_size is not None:
            self._file = tempfile.SpooledTemporaryFile(
                mode=mode,
                max_size=max_size,
                buffering=buffering,
                encoding=encoding,
                newline=newline,
                suffix=suffix,
                prefix=prefix,
                dir=dir,
            )
        else:
            self._file = tempfile.NamedTemporaryFile(
                mode=mode,
                buffering=buffering,
                encoding=encoding,
                newline=newline,
                suffix=suffix,
                prefix=prefix,
                dir=dir,
            )
            self._file.closed

    __exit__ = delegate("_file")
    __enter__ = delegate("_file")
    __iter__ = delegate("_file")

    close = delegate("_file")
    closed = delegate("_file")
    encoding = delegate("_file")
    fileno = delegate("_file")
    flush = delegate("_file")
    isatty = delegate("_file")
    mode = delegate("_file")
    name = delegate("_file")
    newlines = delegate("_file")
    read = delegate("_file")
    readline = delegate("_file")
    readlines = delegate("_file")
    softspace = delegate("_file")
    tell = delegate("_file")
    truncate = delegate("_file")
    write = delegate("_file")
    writelines = delegate("_file")


class TemporaryDirectory(tempfile.TemporaryDirectory):
    """
    Create and return a temporary directory.

    It's just like :class:`~tempfile.TemporaryDirectory`, but yields an
    :class:`File` instead of a `str`.
    """

    def __enter__(self) -> File:  # noqa D105
        return File(super().__enter__())
